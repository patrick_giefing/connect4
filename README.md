# Connect 4 (Multiple implementations - JVM - Python - .NET)

![components](./src/main/document/src/screenshots/component-diagram.png)

## Backend

Postgres (Docker)  
JVM: Gradle, Kotlin, SpringBoot, SpringData, H2 (UnitTest)  
.NET: C#, EntityFramework  
Python: Flask, SqlAlchemy  

## Documentation

[LaTeX (german)](./src/main/document/src/main.pdf)

## GUI (React)

### Create game

![create game](./src/main/document/src/screenshots/create-game.png)

### Game list

![game list](./src/main/document/src/screenshots/game-list.png)

### Select column for tile

The possible positions to drop a tile are marked in the players color. 

![move](./src/main/document/src/screenshots/move.png)

### Winner

Connected tiles are shown with a gold border.

![winner](./src/main/document/src/screenshots/winner.png)

### Tie

There may also be a tie.

![tie](./src/main/document/src/screenshots/tie.png)

### UnitTest

The graphical representation of the UnitTest.

![TestCase](./src/main/document/src/screenshots/testcase.png)