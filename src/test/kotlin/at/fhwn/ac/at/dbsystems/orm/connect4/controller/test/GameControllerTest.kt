package at.fhwn.ac.at.dbsystems.orm.connect4.controller.test

import at.fhwn.ac.at.dbsystems.orm.connect4.controller.GameController
import at.fhwn.ac.at.dbsystems.orm.connect4.controller.PlayerController
import at.fhwn.ac.at.dbsystems.orm.connect4.spring.launcher.Connect4SpringBootStarter
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql

@SpringBootTest(classes = [Connect4SpringBootStarter::class])
@Sql("classpath:create.sql")
class GameControllerTest @Autowired constructor(
    val playerController: PlayerController,
    val gameController: GameController
) {
    @Test
    fun testWin() {
        val players = playerController.getPlayers()
        assertTrue(players.size >= 2, "We need at least 2 players")
        val player1 = players[0]
        val player2 = players[1]
        val game = gameController.startGame(listOf(player1.playerId, player2.playerId), 4, 4)
        gameController.move(game.gameId, player1.playerId, 0)
        gameController.move(game.gameId, player2.playerId, 1)
        gameController.move(game.gameId, player1.playerId, 1)
        gameController.move(game.gameId, player2.playerId, 2)
        gameController.move(game.gameId, player1.playerId, 3)
        gameController.move(game.gameId, player2.playerId, 2)
        gameController.move(game.gameId, player1.playerId, 2)
        gameController.move(game.gameId, player2.playerId, 3)
        gameController.move(game.gameId, player1.playerId, 3)
        gameController.move(game.gameId, player2.playerId, 0)
        val gameFinished = gameController.move(game.gameId, player1.playerId, 3)
        assertEquals(player1.playerId, gameFinished.winner?.playerId)
    }
}