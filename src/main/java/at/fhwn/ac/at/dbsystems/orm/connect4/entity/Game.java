package at.fhwn.ac.at.dbsystems.orm.connect4.entity;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "game")
@Audited
@AuditTable("h_game")
public class Game {
    @Id
    @Column(name = "id_game")
    private String gameId;
    @Column(name = "cols")
    private int cols;
    @Column(name = "rows")
    private int rows;
    @ManyToOne
    @Fetch(FetchMode.SELECT)
    @BatchSize(size = 1000)
    @JoinColumn(name = "id_player_1")
    private Player player1;
    @ManyToOne
    @Fetch(FetchMode.SELECT)
    @BatchSize(size = 1000)
    @JoinColumn(name = "id_player_2")
    private Player player2;
    @ManyToOne
    @Fetch(FetchMode.SELECT)
    @BatchSize(size = 1000)
    @JoinColumn(name = "id_player_winner")
    private Player winner;
    @Column(name = "ts_game_start")
    private LocalDateTime start;
    @Column(name = "ts_game_end")
    private LocalDateTime end;
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_game")
    @Fetch(FetchMode.SELECT)
    @BatchSize(size = 1000)
    @NotAudited
    private List<Move> moves = new ArrayList<>();
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_game")
    @Fetch(FetchMode.SELECT)
    @BatchSize(size = 1000)
    @NotAudited
    private List<ConnectedTile> connectedTiles = new ArrayList<>();

    @PrePersist
    public void prePersist() {
        if (gameId == null) gameId = UUID.randomUUID().toString();
        if (start == null) start = LocalDateTime.now();
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public int getCols() {
        return cols;
    }

    public void setCols(int cols) {
        this.cols = cols;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public Player getWinner() {
        return winner;
    }

    public void setWinner(Player winner) {
        this.winner = winner;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public List<Move> getMoves() {
        return moves;
    }

    public void setMoves(List<Move> moves) {
        this.moves = moves;
    }

    public List<ConnectedTile> getConnectedTiles() {
        return connectedTiles;
    }

    public void setConnectedTiles(List<ConnectedTile> connectedTiles) {
        this.connectedTiles = connectedTiles;
    }
}
