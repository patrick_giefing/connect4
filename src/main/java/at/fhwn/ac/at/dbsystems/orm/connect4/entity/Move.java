package at.fhwn.ac.at.dbsystems.orm.connect4.entity;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "move")
@Audited
@AuditTable("h_move")
public class Move {
    @Embeddable
    public static class MoveId implements Serializable {
        @Column(name = "id_game")
        private String gameId;
        @Column(name = "id_move")
        private int moveId;

        public String getGameId() {
            return gameId;
        }

        public void setGameId(String gameId) {
            this.gameId = gameId;
        }

        public int getMoveId() {
            return moveId;
        }

        public void setMoveId(int moveId) {
            this.moveId = moveId;
        }
    }

    @EmbeddedId
    private MoveId moveId;
    @Column(name = "id_player")
    private String playerId;
    @Column(name = "move_column")
    private int column;
    @Column(name = "ts_move")
    private LocalDateTime moveTime;

    @PrePersist
    public void prePersist() {
        if (moveTime == null) moveTime = LocalDateTime.now();
    }

    public MoveId getMoveId() {
        return moveId;
    }

    public void setMoveId(MoveId moveId) {
        this.moveId = moveId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public LocalDateTime getMoveTime() {
        return moveTime;
    }

    public void setMoveTime(LocalDateTime moveTime) {
        this.moveTime = moveTime;
    }
}
