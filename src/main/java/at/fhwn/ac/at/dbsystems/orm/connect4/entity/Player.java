package at.fhwn.ac.at.dbsystems.orm.connect4.entity;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "player")
@Audited
@AuditTable("h_player")
public class Player {
    @Id
    @Column(name = "id_player")
    private String playerId;
    @Column(name = "name")
    private String name;

    @PrePersist
    public void prePersist() {
        if (playerId == null) playerId = UUID.randomUUID().toString();
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
