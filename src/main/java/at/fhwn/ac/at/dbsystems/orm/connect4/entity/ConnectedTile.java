package at.fhwn.ac.at.dbsystems.orm.connect4.entity;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "connected_tile")
@Audited
@AuditTable("h_connected_tile")
public class ConnectedTile {
    @Embeddable
    public static class ConnectedTileId implements Serializable {
        @Column(name = "id_game")
        private String gameId;
        @Column(name = "tile_column")
        private int column;
        @Column(name = "tile_row")
        private int row;

        public String getGameId() {
            return gameId;
        }

        public void setGameId(String gameId) {
            this.gameId = gameId;
        }

        public int getColumn() {
            return column;
        }

        public void setColumn(int column) {
            this.column = column;
        }

        public int getRow() {
            return row;
        }

        public void setRow(int row) {
            this.row = row;
        }
    }

    @EmbeddedId
    private ConnectedTileId id;

    public ConnectedTileId getId() {
        return id;
    }

    public void setId(ConnectedTileId id) {
        this.id = id;
    }
}
