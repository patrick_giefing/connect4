﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Connect4.Controllers
{
    [ApiController]
    [Route("player")]
    public class PlayerController : ControllerBase
    {
        private readonly Connect4DbContext connect4DbContext = new Connect4DbContext();

        [HttpGet("list")]
        public IEnumerable<Player> GetPlayers()
        {
            return connect4DbContext.Set<Player>();
        }
    }

    [ApiController]
    [Route("game")]
    public class GameController : ControllerBase
    {
        private readonly Connect4DbContext connect4DbContext = new Connect4DbContext();

        [HttpGet("list")]
        public IEnumerable<Game> GetGames()
        {
            return connect4DbContext.Games
                .Include(it => it.Player1)
                .Include(it => it.Player2)
                .Include(it => it.Winner)
                .Include(it => it.Moves)
                .Include(it => it.ConnectedTiles);
        }

        [HttpGet("{gameId}")]
        public Game GetGame(string gameId)
        {
            // TODO how to call "findById" with .Include (see above)
            return GetGames().SingleOrDefault(it => it.GameId == gameId);
        }
    }
}
