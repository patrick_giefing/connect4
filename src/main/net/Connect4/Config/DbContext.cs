using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Connect4
{
    public class Connect4DbContext : DbContext
    {
        public DbSet<Player> Players { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Move> Moves { get; set; }
        public DbSet<ConnectedTile> ConnectedTiles { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("Host=docker-server;Database=connect4;Username=dbuser;Password=1234567");

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Game>(b =>
            {
                b.HasOne(it => it.Player1).WithMany();
                b.HasOne(it => it.Player2).WithMany();
                b.HasOne(it => it.Winner).WithMany();
                b.HasMany(it => it.Moves).WithOne();
                b.HasMany(it => it.ConnectedTiles).WithOne();
            });
            builder.Entity<Move>().HasKey(it => new { it.GameId, it.MoveId });
            builder.Entity<ConnectedTile>().HasKey(it => new { it.GameId, it.Column, it.Row });
        }
    }
}