using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Connect4
{
    [Table(name: "player")]
    public class Player
    {
        [Key]
        [Column(name: "id_player")]
        public string PlayerId { get; set; }
        [Column(name: "name")]
        public string Name { get; set; }
    }

    [Table(name: "game")]
    public class Game
    {
        [Key]
        [Column(name: "id_game")]
        public string GameId { get; set; }
        [Column(name: "ts_game_start")]
        public DateTime Start { get; set; }
        [Column(name: "ts_game_end")]
        public DateTime? End { get; set; }
        [Column(name: "id_player_1")]
        public string Player1Id { get; set; }
        public Player Player1 { get; set; }
        [Column(name: "id_player_2")]
        public string Player2Id { get; set; }
        public Player Player2 { get; set; }
        [Column(name: "id_player_winner")]
        public string WinnerId { get; set; }
        public Player Winner { get; set; }
        [Column(name: "cols")]
        public int Cols { get; set; }
        [Column(name: "rows")]
        public int Rows { get; set; }
        public ICollection<Move> Moves { get; set; }
        public ICollection<ConnectedTile> ConnectedTiles { get; set; }
    }

    [Table(name: "move")]
    public class Move
    {
        [NotMapped]
        public MoveCompositeId Id { get; set; } = new MoveCompositeId();
        [Column(name: "id_game")]
        public string GameId
        {
            get { return Id.GameId; }
            set { Id.GameId = value; }
        }
        [Column(name: "id_move")]
        public int MoveId
        {
            get { return Id.MoveId; }
            set { Id.MoveId = value; }
        }
        [Column(name: "move_column")]
        public int Column { get; set; }
        [Column(name: "ts_move")]
        public DateTime MoveTime { get; set; }
    }

    public class MoveCompositeId
    {
        public string GameId { get; set; }
        public int MoveId { get; set; }
    }

    [Table(name: "connected_tile")]
    public class ConnectedTile
    {
        [NotMapped]
        public ConnectedTileCompositeId Id { get; set; } = new ConnectedTileCompositeId();
        [Key]
        [Column(name: "id_game")]
        public string GameId
        {
            get { return Id.GameId; }
            set { Id.GameId = value; }
        }
        [Key]
        [Column(name: "tile_column")]
        public int Column
        {
            get { return Id.Column; }
            set { Id.Column = value; }
        }
        [Key]
        [Column(name: "tile_row")]
        public int Row
        {
            get { return Id.Row; }
            set { Id.Row = value; }
        }
    }

    public class ConnectedTileCompositeId
    {
        public string GameId { get; set; }
        public int Column { get; set; }
        public int Row { get; set; }
    }
}