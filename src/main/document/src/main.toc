\select@language {english}
\contentsline {section}{\numberline {1}UseCase}{3}
\contentsline {section}{\numberline {2}DB}{3}
\contentsline {section}{\numberline {3}Backend}{5}
\contentsline {subsection}{\numberline {3.1}JVM (Kotlin + Java)}{6}
\contentsline {subsubsection}{\numberline {3.1.1}Entities}{7}
\contentsline {subsubsection}{\numberline {3.1.2}Repos}{8}
\contentsline {subsubsection}{\numberline {3.1.3}Auditing - Hibernate Envers}{9}
\contentsline {subsubsection}{\numberline {3.1.4}Controller}{10}
\contentsline {subsubsection}{\numberline {3.1.5}SQL-Logging}{13}
\contentsline {subsubsection}{\numberline {3.1.6}Test}{15}
\contentsline {subsection}{\numberline {3.2}Python}{17}
\contentsline {subsubsection}{\numberline {3.2.1}Entities}{17}
\contentsline {subsubsection}{\numberline {3.2.2}Controller}{18}
\contentsline {subsection}{\numberline {3.3}C\#}{21}
\contentsline {subsubsection}{\numberline {3.3.1}Entities}{21}
\contentsline {subsubsection}{\numberline {3.3.2}Controller}{23}
\contentsline {section}{\numberline {4}GUI}{24}
