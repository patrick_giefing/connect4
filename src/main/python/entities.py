from sqlalchemy import Integer, Column, create_engine, ForeignKey, String, DateTime
from sqlalchemy.orm import relationship, joinedload, subqueryload, Session
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Player(Base):
    __tablename__ = 'player'
    player_id = Column('id_player', String(36), primary_key=True)
    name = Column()


class Move(Base):
    __tablename__ = 'move'
    game_id = Column('id_game', String(36), ForeignKey('game.id_game'), primary_key=True)
    move_id = Column('id_move', Integer, primary_key=True)
    player_id = Column('id_player', Integer, ForeignKey('player.id_player'))
    column = Column('move_column', Integer)
    move_time = Column('ts_move', DateTime)

    def get_composite_id(self):
        return {
            'gameId': self.game_id,
            'move': self.move_id
        }


class ConnectedTile(Base):
    __tablename__ = 'connected_tile'
    game_id = Column('id_game', String(36), ForeignKey('game.id_game'), primary_key=True)
    column = Column('tile_column', Integer, primary_key=True)
    row = Column('tile_row', Integer, primary_key=True)

    def get_composite_id(self):
        return {
            'gameId': self.game_id,
            'column': self.column,
            'row': self.row
        }


class Game(Base):
    __tablename__ = 'game'
    game_id = Column('id_game', String(36), primary_key=True)
    cols = Column('cols', Integer)
    rows = Column('rows', Integer)
    player_id_1 = Column('id_player_1', Integer, ForeignKey('player.id_player'))
    player_id_2 = Column('id_player_2', Integer, ForeignKey('player.id_player'))
    player_id_winner = Column('id_player_winner', Integer, ForeignKey('player.id_player'))
    player1 = relationship('Player', uselist=False, foreign_keys=[player_id_1], lazy="joined")
    player2 = relationship('Player', uselist=False, foreign_keys=[player_id_2], lazy="joined")
    winner = relationship('Player', uselist=False, foreign_keys=[player_id_winner], lazy="joined")
    start = Column('ts_game_start', DateTime)
    end = Column('ts_game_end', DateTime)
    moves = relationship('Move', uselist=True, foreign_keys=[Move.game_id], lazy="joined")
    connectedTiles = relationship('ConnectedTile', uselist=True, foreign_keys=[ConnectedTile.game_id], lazy="joined")
