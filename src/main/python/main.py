from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from flask import Flask, request, send_from_directory, Response
from controller import *


def send_resource(path):
    return send_from_directory('../resources/static/', path)


def options():
    response = Response("", mimetype='application/json')
    response.headers.add("Access-Control-Allow-Headers", "*")
    response.headers.add("Access-Control-Allow-Methods", "GET,HEAD,POST")
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Allow", "GET,HEAD,POST,PUT,DELETE,OPTIONS,PATCH")
    response.headers.add("Vary", "Origin")
    response.headers.add("Vary", "Access-Control-Request-Method")
    response.headers.add("Vary", "Access-Control-Request-Headers")
    return response


if __name__ == '__main__':
    engine = create_engine('postgresql://dbuser:1234567@docker-server:5432/connect4')
    Session = sessionmaker()
    Session.configure(bind=engine)

    playerController = PlayerController(Session)
    gameController = GameController(Session)

    app = Flask(__name__)
    app.add_url_rule("/<path:path>/", view_func=options, methods=['OPTIONS'])
    app.add_url_rule("/game/start/", view_func=options, methods=['OPTIONS'])
    app.add_url_rule("/game/<game_id>/player/<player_id>/move/<column>/", view_func=options, methods=['OPTIONS'])

    app.add_url_rule("/gui/<path:path>", view_func=send_resource)

    app.add_url_rule("/player/list/", view_func=playerController.get_players, methods=['GET'])
    app.add_url_rule("/player/", view_func=playerController.add_player, methods=['POST'])

    app.add_url_rule("/game/list/", view_func=gameController.get_games, methods=['GET'])
    app.add_url_rule("/game/start/", view_func=gameController.start_game, methods=['POST'])
    app.add_url_rule("/game/<game_id>", view_func=gameController.get_game, methods=['GET'])
    app.add_url_rule("/game/<game_id>/", view_func=gameController.get_game, methods=['GET'])
    app.add_url_rule("/game/<game_id>/player/<player_id>/move/<column>", view_func=gameController.move, methods=['POST'])
    app.add_url_rule("/game/<game_id>/player/<player_id>/move/<column>/", view_func=gameController.move, methods=['POST'])

    app.run(host='0.0.0.0', port=8080)
