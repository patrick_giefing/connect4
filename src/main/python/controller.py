from entities import *
import json
from flask import Response, request, jsonify
from uuid import uuid4
from datetime import datetime


def to_camel_case(snake_str):
    components = snake_str.split('_')
    return components[0] + ''.join(x.title() for x in components[1:])


def map_object(obj):
    if obj is None: return None
    o = vars(obj)
    if hasattr(obj, 'get_composite_id'):
        o['id'] = obj.get_composite_id()
    if '_sa_instance_state' in o:
        del o['_sa_instance_state']
    if 'metadata' in o:
        del o['metadata']
    o_json = {}
    for k in o:
        v = o[k]
        if isinstance(v, datetime):
            v = [v.year, v.month, v.day, v.hour, v.minute, v.second]
        elif isinstance(v, list):
            v = list(map(map_object, v))
        elif isinstance(v, object):
            try:
                v = map_object(v)
            except Exception as ex:
                pass
        o_json[to_camel_case(k)] = v
    return o_json


def dump_list(l):
    l = list(map(map_object, l))
    return json.dumps(l)


def dump_object(o):
    return json.dumps(map_object(o))


def create_json_response(o):
    response = Response(o, mimetype='application/json')
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Access-Control-Allow-Methods", "GET,HEAD,POST")
    response.headers.add("Allow", "GET, HEAD, POST, PUT, DELETE, OPTIONS, PATCH")
    return response


class Controller:
    def __init__(self, Session):
        self.Session = Session


class PlayerController(Controller):
    def __init__(self, Session):
        Controller.__init__(self, Session)

    def get_players(self):
        session = self.Session()
        players = session.query(Player).all()
        return create_json_response(dump_list(players))

    def add_player(self):
        return None, 200


class GameController(Controller):
    def __init__(self, Session):
        Controller.__init__(self, Session)

    def get_games(self):
        session = self.Session()
        games = session.query(Game).all()
        return create_json_response(dump_list(games))

    def start_game(self):
        player_ids = request.get_json()
        cols = request.args.get('cols')
        rows = request.args.get('rows')
        if player_ids is None or not isinstance(player_ids, list) or len(player_ids) != 2:
            return "Expected a list with 2 playerIds in body", 400
        session = self.Session()
        player_id_1 = player_ids[0]
        player_id_2 = player_ids[1]
        player1 = session.query(Player).get(player_id_1)
        player2 = session.query(Player).get(player_id_2)
        game = Game()
        game.game_id = uuid4()
        game.player_id_1 = player_id_1
        game.player_id_2 = player_id_2
        game.player1 = player1
        game.player2 = player2
        game.cols = cols
        game.rows = rows
        game.start = datetime.now()
        session.add(game)
        session.commit()
        return create_json_response(dump_object(game))

    def get_game(self, game_id):
        session = self.Session()
        game = session.query(Game).get(game_id)
        return create_json_response(dump_object(game))

    def move(self, game_id, player_id, column):
        session = self.Session()
        game = session.query(Game).get(game_id)
        if not game: return f'Game not found for id {game_id}', 400
        if game.player1.player_id != player_id and game.player2.player_id != player_id:
            return f'Player with id {player_id} doesn\'t take part in game with id {game_id}', 400
        if game.end is not None:
            return f'Game with id {game_id} already finishe', 400
        move_count = len(game.moves)
        move_count_player = len(list(filter(lambda m: m.player_id == player_id, game.moves)))
        if move_count_player > move_count / 2:
            return 'It\'s the other players turn', 400
        move = Move()
        move.game_id = game_id
        move.move_id = len(game.moves)
        move.player_id = player_id
        move.column = column
        move.move_time = datetime.now()
        game.moves.append(move)
        session.add(move)
        session.commit()
        game = self.check_game_for_winner(game)
        return create_json_response(dump_object(game))

    def check_game_for_winner(self, game):
        # TODO translate kotlin code
        return game
