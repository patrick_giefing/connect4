drop table if exists connected_tile;
drop table if exists move;
drop table if exists game;
drop table if exists player;

create table player
(
    id_player char(36)     NOT NULL PRIMARY KEY,
    name      varchar(100) NOT NULL
);

insert into player
values ('e3de506f-d7b2-417a-891b-25920c9e29a4', 'Alice');
insert into player
values ('eec33e75-8d5a-48a1-be54-d4085f330136', 'Bob');
insert into player
values ('41cafc54-c95d-4dfc-a702-52a8926e3153', 'Charly');

create table game
(
    id_game          char(36)  NOT NULL PRIMARY KEY,
    cols             smallint  NOT NULL,
    rows             smallint  NOT NULL,
    id_player_1      char(36)  NOT NULL references player (id_player) ON DELETE CASCADE ON UPDATE CASCADE,
    id_player_2      char(36)  NOT NULL references player (id_player) ON DELETE CASCADE ON UPDATE CASCADE,
    id_player_winner char(36)  NULL references player (id_player) ON DELETE CASCADE ON UPDATE CASCADE,
    ts_game_start    timestamp NOT NULL,
    ts_game_end      timestamp NULL
);

create table move
(
    id_game     char(36)  NOT NULL references game (id_game) ON DELETE CASCADE ON UPDATE CASCADE,
    id_move     smallint  NOT NULL,
    id_player   char(36)  NULL references player (id_player) ON DELETE CASCADE ON UPDATE CASCADE,
    move_column smallint  NOT NULL,
    ts_move     timestamp NOT NULL,
    PRIMARY KEY (id_game, id_move)
);

create table connected_tile
(
    id_game     char(36) NOT NULL references game (id_game) ON DELETE CASCADE ON UPDATE CASCADE,
    tile_column smallint NOT NULL,
    tile_row    smallint NOT NULL,
    PRIMARY KEY (id_game, tile_column, tile_row)
);

-- history tables

create table if not exists revinfo
(
    rev integer not null
        constraint revinfo_pkey
            primary key,
    revtstmp bigint
);

create table if not exists h_player
(
    id_player varchar(255) not null,
    rev integer not null
        constraint fkfh4yju6sojpg7hi4mig869qxa
            references revinfo,
    revtype smallint,
    name varchar(255),
    constraint h_player_pkey
        primary key (id_player, rev)
);

create table if not exists h_game
(
    id_game varchar(255) not null,
    rev integer not null
        constraint fkf6olw81y4wvtolmq5lrcipyu7
            references revinfo,
    revtype smallint,
    cols integer,
    ts_game_end timestamp,
    rows integer,
    ts_game_start timestamp,
    id_player_1 varchar(255),
    id_player_2 varchar(255),
    id_player_winner varchar(255),
    constraint h_game_pkey
        primary key (id_game, rev)
);

create table if not exists h_move
(
    id_game varchar(255) not null,
    id_move integer not null,
    rev integer not null
        constraint fkan4hgbuie4jiantdyg584cu6e
            references revinfo,
    revtype smallint,
    move_column integer,
    ts_move timestamp,
    id_player varchar(255),
    constraint h_move_pkey
        primary key (id_game, id_move, rev)
);

create table if not exists h_connected_tile
(
    tile_column integer not null,
    id_game varchar(255) not null,
    tile_row integer not null,
    rev integer not null
        constraint fk41e4xtarpd21wm5bnjffpfan0
            references revinfo,
    revtype smallint,
    constraint h_connected_tile_pkey
        primary key (tile_column, id_game, tile_row, rev)
);
