export const getEndoint = () => {
    if (+window.location.port === 3000) {
        return 'http://127.0.0.1:8080/';
    }
    return '/';
}