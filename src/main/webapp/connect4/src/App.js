import {
    HashRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import './App.css';
import CreateGame from "./components/Game/CreateGame";
import Games from "./components/Game/Games";
import Game from "./components/Game/Game";

function App() {
    return (
        <div className="App">
            <Router>
                <Switch>
                    <Route exact path="/game/list" component={Games}/>
                    <Route exact path="/game/create" component={CreateGame}/>
                    <Route exact path="/game/:gameId" component={Game}/>
                </Switch>
                <Redirect from="/" to="/game/list"/>
            </Router>
        </div>
    );
}

export default App;
