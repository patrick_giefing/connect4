import React, {useState, useEffect} from 'react';
import axios from "axios";
import {Redirect} from "react-router-dom";
import {getEndoint} from "../../rest/rest.environment";
import {DataGrid} from "@material-ui/data-grid";
import {Button, ButtonGroup} from "@material-ui/core";
import {Alert} from "@material-ui/lab";

function Games(props) {
    const [games, setGames] = useState([]);
    const [exception, setException] = useState(null);
    const [createGame, setCreateGame] = useState(false);
    const [gameIdSelected, setGameIdSelected] = useState(null);

    const loadGames = () => {
        axios.get(`${getEndoint()}game/list/`).then(response => {
            setException(null);
            setGames(response.data);
        }).catch(reason => {
            setException(reason.message);
            setGames(null);
        });
    };

    const columns = [
        {
            field: 'id',
            headerName: 'ID',
            width: 320,
            valueGetter: params => params.row.gameId
        },
        {
            field: 'player1',
            headerName: 'Player 1',
            width: 120,
            valueGetter: params => params.row.player1.name
        },
        {
            field: 'player2',
            headerName: 'Player 2',
            width: 120,
            valueGetter: params => params.row.player2.name
        },
        {
            field: 'field',
            headerName: 'Field',
            width: 120,
            valueGetter: params => `${params.row.rows} x ${params.row.cols}`
        },
        {
            field: 'status',
            headerName: 'Status',
            width: 200,
            valueGetter: params => params.row.end ? `Finished${params.row.winner ? ` (${params.row.winner.name} won)` : ' (Tie)'}` : 'In progress'
        }
    ];

    useEffect(loadGames, []);

    if (createGame) {
        return (<Redirect to="/game/create"/>);
    }
    if (gameIdSelected) {
        return (<Redirect to={`/game/${gameIdSelected}`}/>);
    }

    return (<div style={{height: 500, textAlign: "left", margin: 10}}>
        {exception ? <Alert severity="error">An error occured: {exception}</Alert> : null}
        <ButtonGroup style={{marginBottom: 10}}>
            <Button onClick={() => setCreateGame(true)}>Create game</Button>
        </ButtonGroup>
        <DataGrid
            rows={games}
            columns={columns}
            onRowSelected={row => setGameIdSelected(row.data.gameId)}
            pageSize={20}
            getRowId={row => row.gameId}
        />
    </div>);
}

export default Games;