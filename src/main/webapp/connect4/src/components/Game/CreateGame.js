import React, {useState, useEffect} from 'react';
import axios from "axios";
import {Button, ButtonGroup, FormControlLabel, Select, TextField} from "@material-ui/core";
import {Alert} from "@material-ui/lab";
import {getEndoint} from "../../rest/rest.environment";
import {Redirect} from "react-router-dom";

function CreateGame(props) {
    const [columns, setColumns] = useState(7);
    const [rows, setRows] = useState(6);
    const [players, setPlayers] = useState([]);
    const [playerId1, setPlayerId1] = useState(null);
    const [playerId2, setPlayerId2] = useState(null);
    const [exception, setException] = useState(null);
    const [returnToGameList, setReturnToGameList] = useState(false);
    const [gameIdCreated, setGameIdCreated] = useState(null);

    const loadPlayers = (year) => {
        axios.get(`${getEndoint()}player/list/`).then(response => {
            const players = response.data;
            setException(null);
            setPlayers(players);
            if (players && players.length > 0) {
                setPlayerId1(players[0].playerId);
                if (players.length > 1) {
                    setPlayerId2(players[1].playerId);
                } else {
                    setPlayerId2(players[0].playerId);
                }
            }
        }).catch(reason => {
            setException(reason.message);
            setPlayers(null);
        });
    }

    const startGame = () => {
        if (!playerId1 || !playerId2) {
            setException("At least one player is missing");
            return;
        }
        axios.post(`${getEndoint()}game/start/`, [playerId1, playerId2], {
            params: {
                cols: columns,
                rows: rows
            }
        }).then(response => {
            setException(null);
            setGameIdCreated(response.data.gameId);
        }).catch(reason => {
            setException(reason.message);
        });
    };

    useEffect(loadPlayers, [])

    if (gameIdCreated) {
        return (<Redirect to={`/game/${gameIdCreated}`}/>);
    }
    if (returnToGameList) {
        return (<Redirect to="/game/list"/>);
    }

    return (<div style={{textAlign: "left"}}>
        {exception ? <Alert severity="error">An error occured: {exception}</Alert> : null}
        <p>
            <FormControlLabel labelPlacement="start"
                              control={<TextField type="number" value={columns}
                                                  onChange={e => setColumns(+e.target.value)} style={{width: 40}}/>}
                              label="Columns:&#160;"/>
            &#160;
            <FormControlLabel labelPlacement="start"
                              control={<TextField type="number" value={rows} onChange={e => setRows(+e.target.value)}
                                                  style={{width: 40}}/>}
                              label="Rows:&#160;"/>
        </p>
        <p>
            <FormControlLabel labelPlacement="start"
                              control={(
                                  <Select native value={playerId1} onChange={e => setPlayerId1(e.target.value)}>
                                      {players?.map(player => <option value={player.playerId}>{player.name}</option>)}
                                  </Select>)}
                              label="Player1:&#160;"/>
        </p>
        <p>
            <FormControlLabel labelPlacement="start"
                              control={(
                                  <Select native value={playerId2} onChange={e => setPlayerId2(e.target.value)}>
                                      {players?.map(player => <option value={player.playerId}>{player.name}</option>)}
                                  </Select>)}
                              label="Player2:&#160;"/>
        </p>
        <p style={{margin: "15px"}}>
            <ButtonGroup>
                <Button onClick={startGame}>Start game</Button>
                <Button onClick={() => setReturnToGameList(true)}>Abbrechen</Button>
            </ButtonGroup>
        </p>
    </div>);
}

export default CreateGame;