import React, {useState, useEffect} from 'react';
import axios from "axios";
import Board from "../Board/Board";
import {getEndoint} from "../../rest/rest.environment";
import {Alert} from "@material-ui/lab";
import "./Game.css";

function Game(props) {
    const gameId = props.match.params.gameId;
    const [game, setGame] = useState({moves: []});
    const [exception, setException] = useState(null);

    const loadGame = () => {
        axios.get(`${getEndoint()}game/${gameId}/`).then(response => {
            setException(null);
            setGame(response.data);
        }).catch(reason => {
            setException(reason.message);
            setGame(null);
        });
    };

    const tileDropped = (playerId, column) => {
        axios.post(`${getEndoint()}game/${gameId}/player/${playerId}/move/${column}/`).then(response => {
            setException(null);
            loadGame();
        }).catch(reason => {
            setException(reason.message);
        });
    };

    useEffect(loadGame, []);
    let playerMessage = null;
    if (game.end) {
        if (game.winner) {
            let playerClassName = game.winner.playerId == game.player1.playerId ? 'player1' : 'player2';
            playerMessage = <span className={playerClassName}>Winner: {game.winner.name}</span>
        } else {
            playerMessage = <span className="tie">Tie</span>
        }
    } else {
        let playerClassName = game.moves.length % 2 == 0 ? 'player1' : 'player2';
        let player = game.moves.length % 2 == 0 ? game.player1 : game.player2;
        if (player) {
            playerMessage = <span className={playerClassName}>Active player: {player.name}</span>
        }
    }

    return (<div>
        {exception ? <Alert severity="error">An error occured: {exception}</Alert> : null}
        {
            game.player1 ?
                <span className="pairing">
                    <span className="player1">{game.player1.name}</span> <span>vs</span> <span
                    className="player2">{game.player2.name}</span> -
                </span>
                : null
        }
        {playerMessage ? <span className="playerMessage">{playerMessage}</span> : null}
        {game ? <Board game={game} tileDropCallback={tileDropped}/> : null}
    </div>);
}

export default Game;