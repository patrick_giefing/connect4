import React, {useState} from 'react';
import PropTypes from "prop-types";
import "./Board.css";
import {Button, ButtonGroup} from "@material-ui/core";
import {Redirect} from "react-router-dom";

function Board(props) {
    const [backToGameList, setBackToGameList] = useState(false);

    const game = props.game;
    let dropTileCells = [];
    let moves = game.moves?.sort((m1, m2) => m1.moveId.moveId > m2.moveId.moveId ? 1 : -1) ?? [];
    const colCounts = [];
    for (let colInd = 0; colInd < game.cols; colInd++) {
        colCounts.push([]);
    }
    for (let move of moves) {
        colCounts[move.column].push(move.playerId);
    }
    let rows = [];
    for (let rowInd = game.rows - 1; rowInd >= 0; rowInd--) {
        let cells = [];
        for (let colInd = 0; colInd < game.cols; colInd++) {
            let colTilesPlayerIds = colCounts[colInd];
            let playerIdTile = colTilesPlayerIds.length >= rowInd ? colTilesPlayerIds[rowInd] : null;
            let cellClassName = "empty";
            let tileClassName = "tile";
            if (playerIdTile === game.player1.playerId)
                cellClassName = "player1";
            else if (playerIdTile === game.player2.playerId)
                cellClassName = "player2";
            if (game.connectedTiles) {
                for (let tile of game.connectedTiles) {
                    if (tile.id.row === rowInd && tile.id.column === colInd)
                        tileClassName += ' connected';
                }
            }
            cells.push(<td className={cellClassName}><span className={tileClassName}></span></td>)
        }
        rows.push(<tr>{cells}</tr>);
    }
    let playerInd = moves.length % 2;
    let playerClassName = `player${playerInd === 0 ? '1' : '2'}`;
    for (let colInd = 0; colInd < game.cols; colInd++) {
        const columnTileCount = moves.filter(move => move.column === colInd).length;
        if (columnTileCount < game.rows && !game.end) {
            dropTileCells.push(<td><span className={`tile ${playerClassName}`}
                                         onClick={() => props.tileDropCallback(playerInd === 0 ? game.player1.playerId : game.player2.playerId, colInd)}></span>
            </td>);
        } else {
            dropTileCells.push(<td><span className="tile disabled"></span></td>);
        }
    }
    if (backToGameList) {
        return (<Redirect to={`/game/list`}/>);
    }
    return (<div className="board">
        <table className="board">
            <tbody className="drop-tile">
            <tr className={playerClassName}>
                {dropTileCells}
            </tr>
            </tbody>
            <tbody className="tiles">
            {rows}
            </tbody>
        </table>
        <ButtonGroup style={{margin: 10}}>
            <Button onClick={() => setBackToGameList(true)}>Back to game list</Button>
        </ButtonGroup>
    </div>);
}

Board.propTypes = {
    game: PropTypes.object.isRequired,
    tileDropCallback: PropTypes.func.isRequired
};

export default Board;