package at.fhwn.ac.at.dbsystems.orm.connect4.spring.launcher

import at.fhwn.ac.at.dbsystems.orm.connect4.ProjectPackageClassPath
import at.fhwn.ac.at.dbsystems.orm.connect4.entity.Game
import at.fhwn.ac.at.dbsystems.orm.connect4.repo.GameRepo
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.web.servlet.config.annotation.*

@SpringBootApplication(scanBasePackageClasses = [ProjectPackageClassPath::class])
@EnableJpaRepositories(basePackageClasses = [GameRepo::class])
@EnableJpaAuditing
@EntityScan(basePackageClasses = [Game::class])
open class Connect4SpringBootStarter {}

@Configuration
@EnableWebMvc
open class WebConfig : WebMvcConfigurer {
    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
    }

    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        registry.addResourceHandler("/gui/**").addResourceLocations("classpath:/static/")
    }
}

fun main(args: Array<String>) {
    runApplication<Connect4SpringBootStarter>(*args)
}
