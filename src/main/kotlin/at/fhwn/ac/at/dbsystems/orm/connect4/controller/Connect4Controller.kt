package at.fhwn.ac.at.dbsystems.orm.connect4.controller

import at.fhwn.ac.at.dbsystems.orm.connect4.entity.ConnectedTile
import at.fhwn.ac.at.dbsystems.orm.connect4.entity.Game
import at.fhwn.ac.at.dbsystems.orm.connect4.entity.Move
import at.fhwn.ac.at.dbsystems.orm.connect4.entity.Player
import at.fhwn.ac.at.dbsystems.orm.connect4.repo.ConnectedTileRepo
import at.fhwn.ac.at.dbsystems.orm.connect4.repo.GameRepo
import at.fhwn.ac.at.dbsystems.orm.connect4.repo.MoveRepo
import at.fhwn.ac.at.dbsystems.orm.connect4.repo.PlayerRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import java.time.LocalDateTime
import org.springframework.web.servlet.view.RedirectView

import org.springframework.web.servlet.mvc.support.RedirectAttributes

import org.springframework.web.bind.annotation.GetMapping

import org.springframework.web.bind.annotation.RequestMapping


@Controller
@RequestMapping("/")
class RedirectController {
    @GetMapping("/")
    fun redirectWithUsingRedirectView(
        attributes: RedirectAttributes
    ): RedirectView {
        attributes.addFlashAttribute("flashAttribute", "redirectWithRedirectView")
        attributes.addAttribute("attribute", "redirectWithRedirectView")
        return RedirectView("/gui/index.html")
    }
}

@Controller
@RestController
@RequestMapping("player")
open class PlayerController @Autowired constructor(val playerRepo: PlayerRepo) {
    @GetMapping("list")
    open fun getPlayers(): List<Player> {
        return playerRepo.findAll();
    }

    @PostMapping
    open fun addPlayer(@RequestBody player: Player) {
        playerRepo.saveAndFlush((player))
    }
}

@Controller
@RestController
@RequestMapping("game")
open class GameController @Autowired constructor(
    val gameRepo: GameRepo,
    val playerRepo: PlayerRepo,
    val moveRepo: MoveRepo,
    val connectedTileRepo: ConnectedTileRepo
) {
    @GetMapping("list")
    open fun getGames(): List<Game> {
        return gameRepo.findAll();
    }

    @PostMapping("start")
    open fun startGame(
        @RequestBody playerIds: List<String>,
        @RequestParam("cols", required = false) cols: Int?,
        @RequestParam("rows", required = false) rows: Int?
    ): Game {
        if (playerIds?.size !== 2) throw Error("Exactly 2 players needed")
        val colCount = 20.coerceAtMost(cols ?: 10)
        val rowCount = 20.coerceAtMost(rows ?: 8)
        if (colCount < 4 || rowCount < 4) throw Error("Less then 4 rows or columns makes it hard for the players to win")
        val game = Game()
        game.player1 = playerRepo.findById(playerIds[0]).orElseThrow { Error("Player 1 doesn't exist") }
        game.player2 = playerRepo.findById(playerIds[1]).orElseThrow { Error("Player 2 doesn't exist") }
        game.cols = colCount
        game.rows = rowCount
        return gameRepo.saveAndFlush(game)
    }

    @GetMapping("{gameId}")
    open fun getGame(@PathVariable("gameId") gameId: String): Game? {
        return gameRepo.findById(gameId).orElse(null)
    }

    @PostMapping("{gameId}/player/{playerId}/move/{column}/")
    open fun move(
        @PathVariable("gameId") gameId: String,
        @PathVariable("playerId") playerId: String,
        @PathVariable("column") column: Int
    ): Game {
        var game = gameRepo.findById(gameId).orElseThrow { Error("Game not found for id $gameId") }
        if (game.player1.playerId != playerId && game.player2.playerId != playerId)
            throw Error("Player with id $playerId doesn't take part in game with id $gameId")
        if (game.end !== null) throw Error("Game with id $gameId already finished, no further move possible")
        val moveCount = game.moves.size
        val moveCountPlayer = game.moves.filter { it.playerId == playerId }.size
        if (moveCountPlayer > moveCount / 2)
            throw Error("It's the other players turn")
        var move = Move()
        val moveId = Move.MoveId()
        moveId.gameId = gameId
        moveId.moveId = game.moves.size
        move.moveId = moveId
        move.playerId = playerId
        move.column = column
        move = moveRepo.saveAndFlush(move)
        game.moves.add(move)
        game = checkGameForWinner(game)
        return game
    }

    @Transactional
    open fun checkGameForWinner(game: Game): Game {
        val moves = game.moves
        moves.sortBy { it.moveId.moveId }
        val moveBoard = mutableListOf<MutableList<String>>()
        for (i: Int in 0 until game.cols) moveBoard.add(mutableListOf())
        for (move in moves) {
            val colPlayerIds = moveBoard[move.column]
            colPlayerIds += move.playerId
        }
        for (columnInd: Int in 0 until game.cols) {
            val columPlayerIds = moveBoard[columnInd]
            while (columPlayerIds.size < game.rows)
                columPlayerIds += ""
        }
        val connectedPaths = mutableListOf<List<List<Int>>>()
        var playerIdConnectedTiles: String? = null
        for (rowInd: Int in 0 until game.rows) {
            for (columnInd: Int in 0 until game.cols) {
                val possiblePaths = mutableListOf<List<List<Int>>>()
                val playerIdTile = moveBoard[columnInd][rowInd]
                if (playerIdTile.isNullOrEmpty()) continue
                if (rowInd < game.rows - 3)
                    possiblePaths += (0..3).map { listOf(columnInd, rowInd + it) }
                if (columnInd >= 3)
                    possiblePaths += (0..3).map { listOf(columnInd - it, rowInd) }
                if (columnInd < game.cols - 3)
                    possiblePaths += (0..3).map { listOf(columnInd + it, rowInd) }
                if (rowInd < game.rows - 3 && columnInd >= 3)
                    possiblePaths += (0..3).map { listOf(columnInd - it, rowInd + it) }
                if (rowInd < game.rows - 3 && columnInd < game.cols - 3)
                    possiblePaths += (0..3).map { listOf(columnInd + it, rowInd + it) }
                val playerTileConnectedPaths =
                    possiblePaths.filter { path -> path.filter { moveBoard[it[0]][it[1]] == playerIdTile }.size === 4 }
                if (playerTileConnectedPaths.isNotEmpty()) {
                    connectedPaths += playerTileConnectedPaths
                    if (playerIdConnectedTiles !== null && playerIdConnectedTiles != playerIdTile) {
                        throw Error("Something went wrong - there are 2 winners")
                    }
                    playerIdConnectedTiles = playerIdTile
                }
            }
        }
        val connectedTiles = connectedPaths.flatten().toSet()
        val connected = connectedPaths.isNotEmpty()
        val noTilesLeft = moves.size == game.rows * game.cols
        if (connected || noTilesLeft) {
            var tiles = connectedTiles.map {
                val tile = ConnectedTile()
                val tileId = ConnectedTile.ConnectedTileId()
                tileId.gameId = game.gameId
                tileId.column = it[0]
                tileId.row = it[1]
                tile.id = tileId
                return@map tile
            }
            tiles = connectedTileRepo.saveAll(tiles)
            game.end = LocalDateTime.now()
            if (connected)
                game.winner = if (playerIdConnectedTiles == game.player1.playerId) game.player1 else game.player2
            val gameSaved = gameRepo.saveAndFlush(game)
            gameSaved.connectedTiles = tiles
            return gameSaved
        }
        return game
    }
}