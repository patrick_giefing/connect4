package at.fhwn.ac.at.dbsystems.orm.connect4.repo

import at.fhwn.ac.at.dbsystems.orm.connect4.entity.ConnectedTile
import at.fhwn.ac.at.dbsystems.orm.connect4.entity.Game
import at.fhwn.ac.at.dbsystems.orm.connect4.entity.Move
import at.fhwn.ac.at.dbsystems.orm.connect4.entity.Player
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
open interface GameRepo : JpaRepository<Game, String> {
    fun findTop3ByEndIsNotNullOrderByEndDesc(): List<Game>

    @Query("select g from Game g " +
                 "where g.player1.name = :playerName or g.player2.name = :playerName")
    fun findGameByPlayerName(@Param("playerName") name: String)

    @Query("select * from game g, player p " +
                 "where g.id_player_winner = p.id_player " +
                 "and p.name = :playerName", nativeQuery = true)
    fun findGameByWinnerName(@Param("playerName")name: String)
}

@Repository
open interface PlayerRepo : JpaRepository<Player, String> {}

@Repository
open interface MoveRepo : JpaRepository<Move, Move.MoveId> {}

@Repository
open interface ConnectedTileRepo : JpaRepository<ConnectedTile, ConnectedTile.ConnectedTileId> {}