const {Sequelize, DataTypes} = require('sequelize');
const sequelize = require('./config/sequelize')

const PlayerRepo = sequelize.define('player', {
    playerId: {
        field: 'id_player',
        type: DataTypes.STRING,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    timestamps: false,
    freezeTableName: true
});

const GameRepo = sequelize.define('game', {
    gameId: {
        field: 'id_game',
        type: DataTypes.STRING,
        primaryKey: true,
        allowNull: false
    },
    cols: {type: DataTypes.INTEGER},
    rows: {type: DataTypes.INTEGER},
    start: {
        field: 'ts_game_start',
        type: DataTypes.DATE
    },
    end: {
        field: 'ts_game_end',
        type: DataTypes.DATE
    }
}, {
    timestamps: false,
    freezeTableName: true
})
GameRepo.belongsTo(PlayerRepo, {foreignKey: 'id_player_1'})
GameRepo.belongsTo(PlayerRepo, {foreignKey: 'id_player_2'})
GameRepo.belongsTo(PlayerRepo, {foreignKey: 'id_player_winner'})

module.exports = {
    PlayerRepo, GameRepo
}