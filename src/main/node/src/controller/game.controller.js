'use strict'
const {Router} = require('express')
const router = Router()
const {GameRepo, PlayerRepo} = require('../repos')

const getGames = async function () {
    const players = await PlayerRepo.findAll()
    const games = await GameRepo.findAll()
    // including model multiple times for different columns doesn't seem to work so dirty workaround
    games.forEach(game => {
        const player1 = players.find(p => p.playerId === game.id_player_1)
        const player2 = players.find(p => p.playerId === game.id_player_2)
        const winner = players.find(p => p.playerId === game.id_player_winner)
        game.dataValues.player1 = player1
        game.dataValues.player2 = player2
        game.dataValues.winner = winner
        delete game.dataValues.id_player_1
        delete game.dataValues.id_player_2
        delete game.dataValues.id_player_winner
    })
    return games
}

router.get('/list', (req, res) => {
    getGames().then(result => res.send(result)).catch(ex => res.send(ex))
})

module.exports = router