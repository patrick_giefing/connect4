'use strict'
const {Router} = require('express')
const router = Router()
const {PlayerRepo} = require('../repos')

router.get('/list', (req, res) => {
    PlayerRepo.findAll().then(result => res.send(result))
})

module.exports = router