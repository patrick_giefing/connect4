'use strict'
const express = require('express')
const createError = require('http-errors')
const PlayerRouter = require('./controller/player.controller')
const GameRouter = require('./controller/game.controller')

const app = express()
app.use('/player', PlayerRouter)
app.use('/game', GameRouter)

app.use((req, res, next) => {
    if (req.method !== 'GET') {
        next(createError(405))
        return
    }
    next(createError(404))
})

app.use((err, req, res, next) => {
    res.status(err.status || 500)
    res.send(err.message)
})

module.exports = app