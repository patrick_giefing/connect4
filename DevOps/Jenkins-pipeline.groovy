pipeline {
    agent any

    stages {
        stage('Git Checkout') {
            steps {
                git url: 'https://bitbucket.org/patrick_giefing/connect4/src'
            }
        }
        stage('Gradle') {
            steps {
                sh './gradlew clean build test'
            }
        }
        stage('Sonarqube') {
            steps {
                sh './gradlew sonarqube -Dsonar.projectKey=connect4 -Dsonar.host.url=http://sonarqube:9000 -Dsonar.login=4b516fd1a2e4a844ac2b09afe4eeea249d509a92'
            }
        }
        stage('NPM') {
            steps {
                echo "Performing npm build..."
                /*withNPM(npmrcConfig:'Node 14') {
                    sh 'npm test'
                }*/
            }
        }
        stage('Reports') {
            steps {
                publishHTML(target: [
                        allowMissing         : false,
                        alwaysLinkToLastBuild: false,
                        keepAll              : true,
                        reportDir            : 'build/reports/tests/test',
                        reportFiles          : 'index.html',
                        reportName           : "Test Report"
                ])
                junit 'build/test-results/test/*.xml'
            }
        }
    }
}
